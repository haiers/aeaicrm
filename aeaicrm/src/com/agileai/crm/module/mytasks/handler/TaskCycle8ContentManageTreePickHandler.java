package com.agileai.crm.module.mytasks.handler;

import java.util.List;

import com.agileai.crm.cxmodule.TaskCycle8ContentManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.TreeBuilder;

public class TaskCycle8ContentManageTreePickHandler
        extends TreeSelectHandler {
    public TaskCycle8ContentManageTreePickHandler() {
        super();
        this.serviceId = buildServiceId(TaskCycle8ContentManage.class);
        this.isMuilSelect = false;
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        List<DataRow> records = getService().queryPickTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(records, "TC_ID", "TC_BEGIN",
                                                  "TC_FID");

        String excludeId = param.get("TC_ID");
        treeBuilder.getExcludeIds().add(excludeId);

        return treeBuilder;
    }

    protected TaskCycle8ContentManage getService() {
        return (TaskCycle8ContentManage) this.lookupService(this.getServiceId());
    }
}
