package com.agileai.crm.module.customer.service;

import java.util.List;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeSelectService;

public interface CustomerSalesPersonnelInfoSelect
        extends TreeSelectService {
	
	public List<DataRow> findChildGroupRecords(String parentId);
}
