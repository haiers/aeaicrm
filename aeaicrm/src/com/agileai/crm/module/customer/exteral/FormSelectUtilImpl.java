package com.agileai.crm.module.customer.exteral;

import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;
import com.agileai.hotweb.ws.BaseRestService;


public class FormSelectUtilImpl extends BaseRestService implements FormSelectUtil {

	@Override
	public String findCodeList(String codeType) {
		String responseText = "";
		try {

			StandardService service = (StandardService) this.lookupService("codeListService");
			List<DataRow> rsList = service.findRecords(new DataParam("TYPE_ID", codeType));
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<rsList.size();i++){
				DataRow dataRow = rsList.get(i);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("codeId", dataRow.get("CODE_ID"));
				jsonObject.put("codeName", dataRow.get("CODE_NAME"));
				jsonArray.put(jsonObject);
			}
			responseText = jsonArray.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}
}
