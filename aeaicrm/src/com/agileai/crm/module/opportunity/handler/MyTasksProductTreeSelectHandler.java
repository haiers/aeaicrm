package com.agileai.crm.module.opportunity.handler;

import java.util.List;

import com.agileai.crm.module.opportunity.service.ProductTreeSelect;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.TreeBuilder;

public class MyTasksProductTreeSelectHandler
        extends TreeSelectHandler {
    public MyTasksProductTreeSelectHandler() {
        super();
        this.serviceId = buildServiceId(ProductTreeSelect.class);
        this.isMuilSelect = true;
        this.checkRelParentNode = false;
        invisiableCheckBoxIdList.add("OPP_CONCERN_PRODUCT");
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        List<DataRow> records = getService().queryPickTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(records, "CODE_ID",
                                                  "CODE_NAME", "TYPE_ID");
        String rootId = "OPP_CONCERN_PRODUCT";
        treeBuilder.setRootId(rootId);

        return treeBuilder;
    }

    protected ProductTreeSelect getService() {
        return (ProductTreeSelect) this.lookupService(this.getServiceId());
    }
}
